//
//  PathPoint.h
//  PathPoints
//
//  Created by Admin on 15.01.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PathPoint : NSObject
@property (nonatomic, strong) NSString *actualAltitude;
@property (nonatomic, strong) NSString *alt;
@property (nonatomic, strong) NSString *difference;
@property (nonatomic, strong) NSString *horizontalAccuracy;
@property (nonatomic, strong) NSString *idObj;
@property (nonatomic, strong) NSString *klat;
@property (nonatomic, strong) NSString *klon;
@property (nonatomic, strong) NSNumber *lat;
@property (nonatomic, strong) NSNumber *lon;
@property (nonatomic, strong) NSString *sessionName;
@property (nonatomic, strong) NSString *speed;
@property (nonatomic, strong) NSNumber *timeStamp;
@property (nonatomic, strong) NSString *verticalAccuracy;

- (instancetype)initWithDict:(NSDictionary *)dict;

/*
"actual_altitude" = "9.411933898925781";
alt = "7.143824";
difference = "-2.268109898925781";
"horizontal_accuracy" = 5;
id = 3906;
klat = "<null>";
klon = "<null>";
lat = "49.228379";
lon = "-122.827244";
"session_name" = Highway;
speed = "28.167862";
"time_stamp" = "1350569139.380652";
"vertical_accuracy" = "9.5";
 */

@end
