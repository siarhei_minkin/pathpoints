//
//  PathPoint.m
//  PathPoints
//
//  Created by Admin on 15.01.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "PathPoint.h"

@implementation PathPoint


/*
 "actual_altitude" = "9.411933898925781";
 alt = "7.143824";
 difference = "-2.268109898925781";
 "horizontal_accuracy" = 5;
 id = 3906;
 klat = "<null>";
 klon = "<null>";
 lat = "49.228379";
 lon = "-122.827244";
 "session_name" = Highway;
 speed = "28.167862";
 "time_stamp" = "1350569139.380652";
 "vertical_accuracy" = "9.5";
 */

- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.actualAltitude = dict[@"actual_altitude"];
        self.alt = dict[@"alt"];
        self.difference = dict[@"difference"];
        self.horizontalAccuracy = dict[@"horizontal_accuracy"];
        self.idObj = dict[@"id"];
        self.klat = dict[@"klat"];
        self.klon = dict[@"klon"];
        self.lat = dict[@"lat"];
        self.lon = dict[@"lon"];
        self.sessionName = dict[@"session_name"];
        self.speed = dict[@"speed"];
        self.timeStamp = dict[@"time_stamp"];
        self.verticalAccuracy = dict[@"vertical_accuracy"];

    }
    return self;
}

@end
