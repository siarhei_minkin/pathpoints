//
//  ViewController.m
//  PathPoints
//
//  Created by Admin on 15.01.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ViewController.h"
@import GoogleMaps;
#import "PathPoint.h"
#import "kalman.h"
#import "gps.h"

@interface ViewController ()  {
    GMSMapView *mapView_;
}

@property (nonatomic, strong) NSMutableArray *points;
@property (nonatomic)  KalmanFilter kalmanFilter;
@property (nonatomic) float noise;
@end

@implementation ViewController

- (void)viewDidLoad {
    // http://stackoverflow.com/questions/1134579/smooth-gps-data
    // https://habrahabr.ru/post/140274/
    // http://reecegriffin.com/blog/where_am_i_ios.html
    // https://github.com/lacker/ikalman
    [super viewDidLoad];
    self.noise = 1;
    self.kalmanFilter = alloc_filter_velocity2d(self.noise); //Noise is 1.0f
    
    [GMSServices provideAPIKey:@"AIzaSyCCMi-2v0D7OSG7IGwQSrtAlZ41EEogNJA"];
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
                                                            longitude:151.20
                                                                 zoom:6];
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.myLocationEnabled = YES;
    self.view = mapView_;
    
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"point_data" ofType:@"json"];
    NSData *myData = [NSData dataWithContentsOfFile:filePath];
    if (myData) {
        NSError *jsonError;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:myData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        self.points = [@[] mutableCopy];
        for (NSDictionary *dict in json) {
            [self.points addObject:[[PathPoint alloc] initWithDict:dict]];
        }
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sessionName == %@", @"Guelph"];
        self.points = [self.points filteredArrayUsingPredicate:predicate];
        
        NSLog(@"%@", json);
    }
    
    PathPoint *firstPoint = self.points[0];
    camera = [GMSCameraPosition cameraWithLatitude:firstPoint.lat.floatValue
                                         longitude:firstPoint.lon.floatValue
                                              zoom:20];
    mapView_.camera = camera;
    
    GMSMutablePath *path = [GMSMutablePath path];
    PathPoint *previousPoint = firstPoint;
    for (PathPoint *point in self.points) {
        update_velocity2d(self.kalmanFilter, point.lat.floatValue, point.lon.floatValue, point.timeStamp.integerValue - previousPoint.timeStamp.integerValue); //Where timeIntervalSinceUpdate is an NSTimeInterval since the last CLLocation
        previousPoint = point;
        
        CLLocationDegrees lat, lon;
        get_lat_long(self.kalmanFilter, &lat, &lon);
        [path addCoordinate:CLLocationCoordinate2DMake(lat, lon)];
       // [path addCoordinate:CLLocationCoordinate2DMake(point.lat.floatValue, point.lon.floatValue)];
    }

    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    polyline.map = mapView_;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
